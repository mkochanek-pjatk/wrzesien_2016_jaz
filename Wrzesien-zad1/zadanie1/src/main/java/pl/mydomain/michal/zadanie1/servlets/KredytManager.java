package pl.mydomain.michal.zadanie1.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.DocumentException;

@WebServlet("kredytManager")
public class KredytManager extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final String PDF_FILE_NAME = "raport.pdf";

	private Kredyt k;
	Path pdfFilePath;

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		response.setContentType("text/html");

		k = new Kredyt();
		k.setKwotaStr(request.getParameter("kwota"));
		k.setIloscRatStr(request.getParameter("iloscrat"));
		k.setStopaStr(request.getParameter("oprocentowanie"));
		k.setOplataStr(request.getParameter("oplata"));
		k.setTypRatyStr(request.getParameter("rodzajrat"));

		if (k.isValidate()) {
			System.out.println("Walidacja - OK. ");
			k.wyliczRatyMalejace();
			try (PrintWriter out = response.getWriter()) {
				out.println("<!DOCTYPE html>");
				out.println("<html>");
				out.println("<head>");
				out.println("<title>Raty kredytu</title>");
				out.println("<Style>");
				out.println("first tr td {");
				out.println("width: 100px;");
				out.println("wight: 600;");
				out.println("}");
				out.println("tr td {");
				out.println("width: 100px;");
				out.println("}");
				out.println("</Style>");
				out.println("</head>");
				out.println("<body>");
				out.println("<h1>Symulacja rat kredytu</h1>");
				out.println("<h2>Raty malej�ce</h2>");

				out.println("<table>");
				out.println("<tbody>");
				out.println("<tr><td>" + k.headers[0] + "</td><td>"
						+ k.headers[1] + "</td><td>" + k.headers[2]
						+ "</td><td>" + k.headers[3] + "</td><td>"
						+ k.headers[4] + "</td><td>" + k.headers[5]
						+ "</td><td>" + k.headers[6] + "</td><td>");

				for (Rata ratka : k.getListaRat()) {
					out.print("<tr><td>"
							+ ratka.getLp().toString()
							+ "</td><td>"
							+ ratka.getTermin().toString()
							+ "</td><td>"
							+ ratka.getKwotaKapitalu()
									.setScale(2, RoundingMode.HALF_UP)
									.toString()
							+ "</td><td>"
							+ ratka.getKwotaOdsetek()
									.setScale(2, RoundingMode.HALF_UP)
									.toString()
							+ "</td><td>"
							+ ratka.getOplatyStale()
									.setScale(2, RoundingMode.HALF_UP)
									.toString()
							+ "</td><td>"
							+ ratka.getKwotaRaty()
									.setScale(2, RoundingMode.HALF_UP)
									.toString()
							+ "</td><td>"
							+ ratka.getSaldoKredytu()
									.setScale(2, RoundingMode.HALF_UP)
									.toString() + "</td></tr>");
				}
				out.println("</tbody>");
				out.println("</table>");
				out.println("<br/>");
				String strPdfFilePath = System.getProperty("user.home") + "/" + PDF_FILE_NAME;
				pdfFilePath = Paths.get(strPdfFilePath);
				if (!Files.exists(pdfFilePath)) {
					System.out.println("File " + strPdfFilePath + " not exists.");
				} else {
					
					out.println("<a href=\"file:///" 
							+ Paths.get(strPdfFilePath).toAbsolutePath() + "\">Pobierz raport PDF</a><BR/>");
					out.println("Je�li powy�szy link nie dzia�a, wklej do przegl�darki poni�szy link: <BR/>"
							+ "file:///" + Paths.get(strPdfFilePath).toAbsolutePath());					
					}
				out.println("</body>");
				out.println("</html>");
			}
		} else {
			response.sendRedirect("/index.jsp");
			System.out.println("Walidacja - b��dy. ");
		}
		ITextPdfGenerator generator = new ITextPdfGenerator();
		try {
			generator.createPdfRaport(pdfFilePath.toString(), k);
		} catch (IOException | DocumentException ex) {
			System.out.println("B��d generatora raportu pdf");
		}
	}
}
