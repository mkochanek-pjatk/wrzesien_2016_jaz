package pl.mydomain.michal.zadanie1.servlets;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.RoundingMode;

public class ITextPdfGenerator {
	  private static Font bigBlueFont = new Font(Font.FontFamily.HELVETICA, 18,
		      Font.BOLD, BaseColor.BLUE);
		  private static Font blueFont = new Font(Font.FontFamily.HELVETICA, 14,
		      Font.NORMAL, BaseColor.BLUE);
		  private static Font boldFont = new Font(Font.FontFamily.HELVETICA, 12,
		      Font.BOLD);
		  private static Font smallBold = new Font(Font.FontFamily.HELVETICA, 10,
		      Font.BOLD);

	private static void addMetaData(Document document) {
		document.addTitle("Kredyt");
		document.addSubject("Using iText");
		document.addLanguage("pl");
	}

	public void createPdfRaport(String dest, Kredyt k) throws IOException,
			DocumentException {
		Document document = new Document();
		FileOutputStream fstream = new FileOutputStream(dest);
		try {
			PdfWriter.getInstance(document, fstream);
			document.open();

			document.add(new Paragraph("Symulacja rat kredytu", bigBlueFont));
			document.add(new Paragraph("Raty malej�ce", blueFont));
			document.add(new Paragraph(" "));

			PdfPTable table = new PdfPTable(7);
			for (int i = 0; i <= 6; i++) {
				table.addCell(new Paragraph (k.headers[i].toString(), smallBold));
			}
			int maxRowCount = k.getListaRat().size();
			for (int i = 0; i < maxRowCount; i++) {
				table.addCell(k.getListaRat().get(i).getLp().toString());
				table.addCell(k.getListaRat().get(i).getTermin().toString());
				table.addCell(k.getListaRat().get(i).getKwotaKapitalu()
						.setScale(2, RoundingMode.HALF_UP).toString());
				table.addCell(k.getListaRat().get(i).getKwotaOdsetek()
						.setScale(2, RoundingMode.HALF_UP).toString());
				table.addCell(k.getListaRat().get(i).getOplatyStale()
						.setScale(2, RoundingMode.HALF_UP).toString());
				table.addCell(k.getListaRat().get(i).getKwotaRaty()
						.setScale(2, RoundingMode.HALF_UP).toString());
				table.addCell(k.getListaRat().get(i).getSaldoKredytu()
						.setScale(2, RoundingMode.HALF_UP).toString());
			}
			document.add(table);
		} catch (DocumentException ex) {
			System.out.println("DocumentException");
			throw new DocumentException(ex);
		} finally {
			document.close();
			fstream.close();
		}
	}
}