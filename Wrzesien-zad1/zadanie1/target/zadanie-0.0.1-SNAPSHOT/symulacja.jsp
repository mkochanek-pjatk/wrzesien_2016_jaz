<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>	
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Response page</title>
<style>
tr td {
width: 100px;
}
</style>
</head>
<body>
	<h1>Symulacja rat kredytu.</h1>
	<table>
	<tr><td>lp</td><td>termin</td><td>kwotaKapitalu</td><td>kwotaOdsetek</td><td>kwotaRaty</td><td>saldoKredytu</td></tr>
	
        <c:forEach items="${k.getListaRat}" var="ratka" varStatus="e">
        <tr><td>${ratka.lp}</td><td>${ratka.termin}</td><td>${ratka.kwotaKapitalu}</td><td>${ratka.kwotaOdsetek}</td>
        <td>${ratka.kwotaRaty}</td><td>${ratka.saldoKredytu}</td></tr>
        </c:forEach>	
	</table>

</body>
</html>