package calculator;

public class CountDzieloBrutto{

	private double brutto;
	private double emerytalna;
	private double rentowa;
	private double chorobowe;
	private double zdrowotne;
	private double podstawa;
	private double zaliczkaPIT;
	private double netto;
	private double kwotaPoOdtraceniu;
	private double doOdliczenia;
	private double zaliczkaPITdoZaplaty;
	private double kosztUzPrzy;

	private String kosztyUzyskaniaPrzychodu;

	public CountDzieloBrutto(double brutto,String kosztyUzyskaniaPrzychodu) {
		this.brutto = brutto;
		this.kosztyUzyskaniaPrzychodu = kosztyUzyskaniaPrzychodu;
		
		if(kosztyUzyskaniaPrzychodu.equals("twenty"))kosztUzPrzy=(brutto-(chorobowe+emerytalna))*(0.2);
		else if(kosztyUzyskaniaPrzychodu.equals("fifty"))kosztUzPrzy=(brutto-(chorobowe+emerytalna))*(0.5);
		
		podstawa=(brutto-(chorobowe+emerytalna))-kosztUzPrzy;
		zaliczkaPIT=((brutto-(chorobowe+emerytalna))-kosztUzPrzy)*0.18;

		zaliczkaPITdoZaplaty=(((brutto-(chorobowe+emerytalna))-kosztUzPrzy)*0.18)-(brutto*0.0775);
		netto=brutto-(zaliczkaPITdoZaplaty+emerytalna+chorobowe+zdrowotne);
	}
	public double getBrutto() {
		return brutto;
	}
	public void setBrutto(double brutto) {
		this.brutto = brutto;
	}
	public double getEmerytalna() {
		return emerytalna;
	}
	public void setEmerytalna(double emerytalna) {
		this.emerytalna = emerytalna;
	}
	public double getRentowa() {
		return rentowa;
	}
	public void setRentowa(double rentowa) {
		this.rentowa = rentowa;
	}
	public double getChorobowe() {
		return chorobowe;
	}
	public void setChorobowe(double chorobowe) {
		this.chorobowe = chorobowe;
	}
	public double getZdrowotne() {
		return zdrowotne;
	}
	public void setZdrowotne(double zdrowotne) {
		this.zdrowotne = zdrowotne;
	}
	public double getPodstawa() {
		return podstawa;
	}
	public void setPodstawa(double podstawa) {
		this.podstawa = podstawa;
	}
	public double getZaliczkaPIT() {
		return zaliczkaPIT;
	}
	public void setZaliczkaPIT(double zaliczkaPIT) {
		this.zaliczkaPIT = zaliczkaPIT;
	}
	public double getNetto() {
		return netto;
	}
	public void setNetto(double netto) {
		this.netto = netto;
	}
	public double getKwotaPoOdtraceniu() {
		return kwotaPoOdtraceniu;
	}
	public void setKwotaPoOdtraceniu(double kwotaPoOdtraceniu) {
		this.kwotaPoOdtraceniu = kwotaPoOdtraceniu;
	}
	public double getDoOdliczenia() {
		return doOdliczenia;
	}
	public void setDoOdliczenia(double doOdliczenia) {
		this.doOdliczenia = doOdliczenia;
	}
	public double getZaliczkaPITdoZaplaty() {
		return zaliczkaPITdoZaplaty;
	}
	public void setZaliczkaPITdoZaplaty(double zaliczkaPITdoZaplaty) {
		this.zaliczkaPITdoZaplaty = zaliczkaPITdoZaplaty;
	}
	public double getKosztUzPrzy() {
		return kosztUzPrzy;
	}
	public void setKosztUzPrzy(double kosztUzPrzy) {
		this.kosztUzPrzy = kosztUzPrzy;
	}
}
