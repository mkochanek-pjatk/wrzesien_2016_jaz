package calculator;

public class CountPracaNetto {

	private double brutto;
	private double emerytalna;
	private double rentowa;
	private double chorobowe;
	private double zdrowotne;
	private double podstawa;
	private double zaliczkaPIT;
	private double netto;
	private double kwotaPoOdtraceniu;
	private double doOdliczenia;
	private double zaliczkaPITdoZaplaty;
	private String wynik;
	private String[]months=new String[12];
	
	public String getMonths(int i) {
		return months[i];
	}
	public void setMonths(String[] months) {
		this.months = months;
	}
	public CountPracaNetto(double netto) {
		this.netto = netto;
		brutto=netto/0.695439;
		emerytalna=brutto*(0.0976);
		rentowa=brutto*(0.015);
		chorobowe=brutto*(0.0245);
		zdrowotne=brutto*(0.0775);
		podstawa=brutto-(emerytalna+rentowa+chorobowe+zdrowotne);
		zaliczkaPIT=(brutto*(0.18))-46.33;
		kwotaPoOdtraceniu=(brutto)-(emerytalna+rentowa+chorobowe);
		doOdliczenia=kwotaPoOdtraceniu*0.0775;
		zaliczkaPITdoZaplaty=zaliczkaPIT-doOdliczenia;
		netto=podstawa-zaliczkaPITdoZaplaty;
		
		months[0]="styczen";
		months[1]="luty";
		months[2]="marzec";
		months[3]="kwiecien";
		months[4]="maj";
		months[5]="czerwiec";
		months[6]="lipiec";
		months[7]="sierpien";
		months[8]="wrzesien";
		months[9]="pazdziernik";
		months[10]="listopad";
		months[11]="grudzien";
	}

	public double getBrutto() {
		return brutto;
	}
	public void setBrutto(double brutto) {
		this.brutto = brutto;
	}
	public double getEmerytalna() {
		return emerytalna;
	}
	public void setEmerytalna(double emerytalna) {
		this.emerytalna = emerytalna;
	}
	public double getRentowa() {
		return rentowa;
	}
	public void setRentowa(double rentowa) {
		this.rentowa = rentowa;
	}
	public double getChorobowe() {
		return chorobowe;
	}
	public void setChorobowe(double chorobowe) {
		this.chorobowe = chorobowe;
	}
	public double getZdrowotne() {
		return zdrowotne;
	}
	public void setZdrowotne(double zdrowotne) {
		this.zdrowotne = zdrowotne;
	}
	public double getPodstawa() {
		return podstawa;
	}
	public void setPodstawa(double podstawa) {
		this.podstawa = podstawa;
	}
	public double getZaliczkaPIT() {
		return zaliczkaPIT;
	}
	public void setZaliczkaPIT(double zaliczkaPIT) {
		this.zaliczkaPIT = zaliczkaPIT;
	}
	public double getNetto() {
		return netto;
	}
	public void setNetto(double netto) {
		this.netto = netto;
	}
	public String getWynik() {
		return wynik;
	}
	public void setWynik(String wynik) {
		this.wynik = wynik;
	}
	public double getKwotaPoOdtraceniu() {
		return kwotaPoOdtraceniu;
	}
	public void setKwotaPoOdtraceniu(double kwotaPoOdtraceniu) {
		this.kwotaPoOdtraceniu = kwotaPoOdtraceniu;
	}
	public double getDoOdliczenia() {
		return doOdliczenia;
	}
	public void setDoOdliczenia(double doOdliczenia) {
		this.doOdliczenia = doOdliczenia;
	}
	public double getZaliczkaPITdoZaplaty() {
		return zaliczkaPITdoZaplaty;
	}
	public void setZaliczkaPITdoZaplaty(double zaliczkaPITdoZaplaty) {
		this.zaliczkaPITdoZaplaty = zaliczkaPITdoZaplaty;
	}
}
