package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/zlecenie")
public class ZlecenieServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		out.println("<html><body><h2>Dodatkowy formularz - umowa zlecenie</h2>"
				+"Forma umowy: "
				+request.getSession().getAttribute("contractKind")
				+ "<form action='zlecenieSuccess'>" 
				
				+ "<fieldset id='skladkaRentowa'name='skladkaRentowa'>Skladka rentowa<br>"
				+ "<input type='radio' name='skladkaRentowa' value='yes' checked='checked'/>tak<br>"
				+ "<input type='radio' name='skladkaRentowa' value='no'/>nie<br>"
				+ "</fieldset>"
				+ "<fieldset id='skladkaEmerytalna'name='skladkaEmerytalna'>Skladka emerytalna<br>"
				+ "<input type='radio' name='skladkaEmerytalna' value='yes' checked='checked'/>tak<br>"
				+ "<input type='radio' name='skladkaEmerytalna' value='no'/>nie<br>"
				+ "</fieldset>"
				+ "<fieldset id='skladkaChorobowa'name='skladkaChorobowa'>Skladka chorobowa<br>"
				+ "<input type='radio' name='skladkaChorobowa' value='yes' checked='checked'/>tak<br>"
				+ "<input type='radio' name='skladkaChorobowa' value='no'/>nie<br>"
				+ "</fieldset>"
				+ "<fieldset id='skladkaZdrowotna'name='skladkaZdrowotna'>Skladka zdrowotna<br>"
				+ "<input type='radio' name='skladkaZdrowotna' value='yes' checked='checked'/>tak<br>"
				+ "<input type='radio' name='skladkaZdrowotna' value='no'/>nie<br>"
				+ "</fieldset>"
				+ "<fieldset id='kosztyUzyskaniaPrzychodu'name='kosztyUzyskaniaPrzychodu'>Koszty uzyskania przychodu<br>"
				+ "<input type='radio' name='kosztyUzyskaniaPrzychodu' value='twenty' checked='checked'/>20%<br>"
				+ "<input type='radio' name='kosztyUzyskaniaPrzychodu' value='fifty'/>50%<br>"
				+ "</fieldset>"
					+ "<input type='submit' value='nastepny krok'/><br>"
				+ "</form>"
				
			+ "</body></html>");
		out.close();
	}
}