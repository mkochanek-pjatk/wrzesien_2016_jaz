package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/dzielo")
public class DzieloServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		out.println("<html><body><h2>Dodatkowy formularz - umowa o dzielo</h2>"
				+"Forma umowy: "
				+request.getSession().getAttribute("contractKind")
				+ "<form action='dzieloSuccess'>" 
				+ "<fieldset id='kosztyUzyskaniaPrzychodu'name='kosztyUzyskaniaPrzychodu'>Koszty uzyskania przychodu<br>"
				+ "<input type='radio' name='kosztyUzyskaniaPrzychodu' value='twenty'checked='checked'/>20%<br>"
				+ "<input type='radio' name='kosztyUzyskaniaPrzychodu' value='fifty'/>50%<br>"
				+ "</fieldset>"
					+ "<input type='submit' value='nastepny krok'/><br>"
				+ "</form>"
			+ "</body></html>");
		out.close();
	}
}