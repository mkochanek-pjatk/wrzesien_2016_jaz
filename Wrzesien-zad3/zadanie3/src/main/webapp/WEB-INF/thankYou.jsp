<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="domain.Contract"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<jsp:useBean id="contract" class="domain.Contract" scope="session"/>
<jsp:setProperty property="contractKind" name="contract" param="contractKind"/>
<jsp:setProperty property="year" name="contract" param="year"/>
<jsp:setProperty property="quotaType" name="contract" param="quotaType"/>
<jsp:setProperty property="amount" name="contract" param="amount"/>

contractKind <jsp:getProperty property="contractKind"name="contract"/>
year <jsp:getProperty property="year" name="contract"/>
quotaType <jsp:getProperty property="quotaType" name="contract"/>
amount <jsp:getProperty property="amount" name="contract"/>

</body>
</html>