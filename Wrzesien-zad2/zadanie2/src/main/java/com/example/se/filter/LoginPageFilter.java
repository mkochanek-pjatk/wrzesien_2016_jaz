package com.example.se.filter;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginPageFilter implements Filter {

	FilterConfig filterConfig = null;

	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {

		HttpServletResponse rs = (HttpServletResponse) res;
		HttpServletRequest rq = (HttpServletRequest) req;

		PrintWriter out = res.getWriter();
		HttpSession session = rq.getSession();

		try {
			String per = session.getAttribute("permision").toString();
			if (per.equals("Admin") || per.equals("Premium") || per.equals("Normal")) {
				rs.sendRedirect("profile.jsp");
			}
		} catch (NullPointerException e) {
			chain.doFilter(req, res);
			String message = "<p>Nie zalogowano <a href='index.jsp'>zaloguj</a></p>";
			out.print(message);
		}
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

}