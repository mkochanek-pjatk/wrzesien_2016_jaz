package com.example.se.dao;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.se.domain.Person;
import com.example.se.domain.Privilages;
import com.example.se.domain.address.Address;
import com.example.se.domain.address.Province;
import com.example.se.domain.address.Type;

public class DAO{
	private final static String DBURL = "jdbc:hsqldb:hsql://localhost";
	private final static String DBUSER = "sa";
	private final static String DBPASS = "";
	private final static String DBDRIVER = "org.hsqldb.jdbcDriver";

	private Connection connection;
	private Statement statement;
	private String query;
	private SQLQueryGenerator generator;
	private ResultSet result;
	private PersonBuilder personBuilder;
	private PrintWriter out;
	private boolean isConnected = false;

	public DAO() throws Exception {
		Class.forName(DBDRIVER).newInstance();
		generator = new SQLQueryGenerator();
		personBuilder = new PersonBuilder();
		try {
			connection = DriverManager.getConnection(DBURL, DBUSER, DBPASS);
			statement = connection.createStatement();
			isConnected = true;
		} catch (SQLException e) {
			System.err.println("ERROR: Nie można podłączyć się do bazy");
		}
	}

	public int insert(Person person) {
		this.query = generator.insert(person);
		int z = 0;
		try {
			if (isConnected == true) {
				z = this.statement.executeUpdate(this.query);
			} else {
				out.print("<h2>ERROR: Databace not connected</h2>");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return z;
	}

	public void edit(Person person, String id) {
		this.query = generator.update(person, id);
		try {
			this.statement.executeQuery(this.query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void delete(String id) {
		this.query = generator.delete(id);
		try {
			this.statement.executeQuery(this.query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public int getByLogin(String login) {
		int i = 0;
		this.query = generator.ifExist(login);
		try {
			if (isConnected == true) {
				this.result = this.statement.executeQuery(query);
				result.next();
				i = result.getInt(1);
			} else {
				out.print("<h2>ERROR: Databace not connected</h2>");
			}
		} catch (SQLException e) {
			out.print("Cannot connect to database");
		}
		return i;
	}

	public Map<String, Person> getAll() {
		this.query = generator.getAll();
		Map<String, Person> persons = new HashMap<>();
		try {
			this.result = this.statement.executeQuery(this.query);

			while (result.next()) {
				Person person = new Person();
				person.setLogin(result.getString(2));
				person.setPassword(result.getString(3));
				person.setEmail(result.getString(4));
				person.setPrivilages(Privilages.valueOf(result.getString(5)));
				persons.put(result.getString(1), person);
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
		return persons;
	}

	public Person get(String login, String password) {
		Person p = null;
		int count = 0;
		this.query = generator.selectLoginPass(login, password);
		if (isConnected == true) {
			try {
				this.result = this.statement.executeQuery(query);
				this.connection.close();

				if (this.result.next()) {
					count = result.getInt(6);
					if (count == 1) {
						p = new Person();
						p = this.personBuilder.build(this.result);
					}
				}
			} catch (Exception e) {
				out.println("invaild login");
			}
		} else {
			out.print("<h2>ERROR: Databace not connected</h2>");
		}
		return p;
	}

	public void setPrintWriter(PrintWriter w) {
		this.out = w;
	}

	private String getUserIdByLogin(String login) throws SQLException {
		this.query = generator.getIdByLogin(login);
		ResultSet rs = this.statement.executeQuery(query);

		String id = "";
		while (rs.next())
			id = rs.getString("id");
		return id;
	}

	public List<Address> getUserAddressesByLogin(String userName) throws SQLException {
		String userId = this.getUserIdByLogin(userName);
		System.out.println(userId);
		this.query = generator.getUserAddresses(userId);
		ResultSet rs = this.statement.executeQuery(this.query);
		List<Address> addresses = new ArrayList<>();
		while (rs.next()) {
			Address address = new Address();
			address.setId(rs.getInt("Id"));
			address.setType(Type.valueOf(rs.getString("AddressType")));
			address.setProvince(Province.valueOf(rs.getString("Province")));
			address.setCity(rs.getString("City"));
			address.setZipCode(rs.getString("ZipCode"));
			address.setStreet(rs.getString("Street"));
			address.setHouseNumber(rs.getString("House"));
			addresses.add(address);
		}

		return addresses;
	}

	public void deleteAddress(String id) throws SQLException {
		this.query = generator.deleteAddress(id);
		this.statement.executeUpdate(this.query);
	}

	public void editAddress(Address address) throws SQLException {
		this.query = generator.editAddress(address);
		this.statement.executeUpdate(this.query);
	}
	
	public void insertAddress(Address address, String user) throws SQLException{
		String userID = getUserIdByLogin(user);
        this.query = generator.addAddress(address, userID);
        try {
			this.statement.executeUpdate(this.query);
		} catch (SQLException e) {
			System.out.println("ERR in dao");
		}
    }

}
