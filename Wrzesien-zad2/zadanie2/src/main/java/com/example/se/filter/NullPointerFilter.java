package com.example.se.filter;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class NullPointerFilter implements Filter {

	FilterConfig filterConfig = null;

	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {

		String login = "<p>Brak uprawnien</p><a href='success.jsp'>Powrot</a>";

		HttpServletResponse rs = (HttpServletResponse) res;
		HttpServletRequest rq = (HttpServletRequest) req;
	
		try{
		String r = req.getParameter("login");
		chain.doFilter(req,res);
		}
		catch(Exception e){
			if(rq.getRequestURI().toString().contains("editData")){
				rs.sendRedirect("admin.jsp");
			}
			if(rq.getRequestURI().toString().contains("addressPage.jsp")){
				rs.sendRedirect("index.jsp");
			}
		}
		
	}

	public void destroy() {
		// add code to release any resource
	}
}