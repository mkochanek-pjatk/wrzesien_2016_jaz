import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.example.se.dao.DAO;
import com.example.se.dao.PersonBuilder;
import com.example.se.domain.Person;
import com.example.se.domain.Privilages;

@WebServlet(urlPatterns = "/registration")
public class registration extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");

		HttpSession session = request.getSession();
		PrintWriter out = response.getWriter();

		String login = request.getParameter("login");
		String pass = request.getParameter("pass");
		String email = request.getParameter("email");

		if ((login != "") && (login != null) && (login != "null") && (pass != "") && (pass != null)
				&& (pass != "null") && (email != "") && (email != null) && (email != "null")) {

			DAO db = null;
			int count = 2;
			try {
				db = new DAO();
				count = db.getByLogin(login);
			} catch (Exception e) {
				out.println("<p>ERROR: Database not connected, try later <a href=index.jsp>Home</a></p>");
			}
			
//			int count = db.getByLogin(login);

			if (count == 0) {
				PersonBuilder personBuilder = new PersonBuilder();
				Person person = personBuilder.build(login, pass, email, Privilages.Normal.name());
				int z = db.insert(person);
				if (z == 1) {
					out.print(
							"<p>Registration is Successful.Please Login Here</p><a href='index.jsp'>Go to Login</a>");
				}
			} else if (count == 1)  {
				out.print("Uzytkownik o takiej nazwie istnieje. Uzyj innej nazwy");
				response.setHeader("Refresh", "2;url=reg.jsp");
			}

		}
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {}
}