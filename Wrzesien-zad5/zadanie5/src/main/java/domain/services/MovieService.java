package domain.services;
	
	import java.util.ArrayList;
	import java.util.List;
	
	import domain.Movie;
	
	public class MovieService {
	
		private static List<Movie> db = new ArrayList<Movie>();
		private static int currentId = 1;
		public List<Movie> getAll(){
			return db;
		}
		
		public Movie get(int id){
			for(Movie p : db){
				if(p.getId()==id)
					return p;
			}
			return null;
		}
		
		public void add(Movie p){
			p.setId(++currentId);
			db.add(p);
		}
		
		public void update(Movie movie){
			for(Movie p : db){
				if(p.getId()==movie.getId()){
					p.setTitle(movie.getTitle());
					p.setDirector(movie.getDirector());
					p.setWriter(movie.getWriter());
				}
			}
		}
		
		public void delete(Movie p){
			db.remove(p);
		}
		
	}
